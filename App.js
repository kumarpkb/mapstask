import React, { useState } from 'react';
import {
  StyleSheet,
  Dimensions,
  TextInput,
  View
} from 'react-native';
import MapView from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 13.111220;
const LONGITUDE = 77.608391;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const GOOGLE_MAPS_APIKEY = "AIzaSyBCOZt4mSv9SnVCPjBMyhDWDGqLN_K4Ggc";

const App = () => {


  const origin = { "0": 12.969850, "1": 77.751038 };
  const destination = { "0": 13.111220, "1": 77.608391 };
  const [originState, setOriginState] = useState({ ...origin })
  const [destinationState, setDestinationState] = useState({ ...destination })

  const handleClickStarting = (value) => {
    setOriginState(prevState => ({ ...prevState, ...value.split(',') }))
  }
  const handleClickDestination = (value) => {
    setDestinationState(prevState => ({ ...prevState, ...value.split(',') }))
  }


  return (
    <View style={styles.container}>
      <MapView
        initialRegion={{
          latitude: LATITUDE,
          longitude: LONGITUDE,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        }}
        style={styles.map}
      >
        <MapView.Marker coordinate={{
          latitude: originState[0] == "" ? 12.969850 : parseFloat(originState[0]),
          longitude: originState[1] == "" ? 77.751038 : parseFloat(originState[1])
        }} />
        <MapView.Marker coordinate={{
          latitude: destinationState[0] == "" ? 13.111220 : parseFloat(destinationState[0]),
          longitude: destinationState[1] == "" ? 77.608391 : parseFloat(destinationState[1])
        }} />
        <MapViewDirections
          origin={{
            latitude: originState[0] == "" ? 12.969850 : parseFloat(originState[0]),
            longitude: originState[1] == "" ? 77.751038 : parseFloat(originState[1])
          }}
          destination={{
            latitude: destinationState[0] == "" ? 13.111220 : parseFloat(destinationState[0]),
            longitude: destinationState[1] == "" ? 77.608391 : parseFloat(destinationState[1])
          }}
          apikey={GOOGLE_MAPS_APIKEY}
          strokeWidth={30}
          strokeColor="red"
        />
      </MapView>
      <View style={styles.inputView}>
        <TextInput
          defaultValue={"12.969850" + "," + "77.751038"}
          onChangeText={handleClickStarting}
          placeholder="Starting Point"
          style={styles.input}
          keyboardType="numeric"
        />
        <TextInput
          defaultValue={"13.111220" + "," + "77.608391"}
          onChangeText={handleClickDestination}
          placeholder="Destination Point"
          keyboardType="numeric"
          style={styles.input} />
      </View>
    </View>
  );
};

var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  map: {
    flex: 1,
  },
  inputView: {
    backgroundColor: 'rgba(0,0,0,0)',
    position: 'absolute',
    top: 0,
    left: 5,
    right: 5
  },
  input: {
    height: 46,
    padding: 10,
    marginTop: 20,
    marginLeft: 10,
    marginRight: 10,
    fontSize: 18,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: 'red',
    backgroundColor: 'white',
  }
})

export default App;
